const axios = require("axios"),
  appRoutes = require("../routes.json");
//   axios.get('https://jsonplaceholder.typicode.com/users')
//   .then(res => {
//     let routes = res.data.map(user => '/users/' + user.username)
//     callback(null, routes)
//   })
//   .catch(callback)
module.exports = async function getAppRoutes() {
  // Initiate axios
  const instance = axios.create({
    baseURL: "http://localhost:1337",
  });

  // Fetch available languages and store their codes (fr, en, ...) in array
  const availableLanguages = await axios
    .get("/api/v1/product/find-least-products")
    .then(({ data }) => data.map(product => product.name));

  // Fetch blogPosts as object with languages as attributes and slugs as their values
  const blogPosts = fetchBlogPosts();

  const routes = [];

  // for every language
  for (let i = 0; i < availableLanguages.length; i += 1) {
    // and every route defined in routes.json
    for (let k = 0; k < appRoutes.length; k += 1) {
      let routePath = appRoutes[k];
      const language = availableLanguages[i];

      // replace the language placeholder with current language
      routePath = routePath.replace(/:category/, language);

      // If the route includes 'blog', iterate over all fetched posts in current language
      if (routePath.includes("/blog/")) {
        for (
          let postIndex = 0;
          postIndex < blogPosts[language].length;
          postIndex += 1
        ) {
          routes.push(
            routePath.replace(/:subCategory/, blogPosts[language][postIndex])
          );
        }
      }

      // could add more 'else if' conditions to account for other type of pages (than just blog)

      // Otherwise it's home page and just push it
      else {
        routes.push(routePath);
      }
    }
  }

  // Return all available routes
  return routes;
};
