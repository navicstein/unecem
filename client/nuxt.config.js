const nodeExternals = require("webpack-node-externals");
// const getAppRoutes = require("./utils/getRoutes.js");
export default {
  mode: "universal",
  server: {
    port: 3000, // default: 3000
    host: "0.0.0.0" // default: localhost
  },
  env: {
    baseUrl: process.env.BASE_URL || "http://localhost:3000"
  },
  // router: {
  //   middleware: "validate"
  // },
  // loading: "~/components/loading.vue",
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: "%s | " + process.env.npm_package_name,
    title: "Buy special Nigerian made goods Online",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css?family=Montserrat&display=swap"
      },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: "green",
    height: "15px"
    // loading: '~/components/loading.vue'
  },
  /*
   ** Global CSS
   */
  css: ["@/assets/style/animate.css"],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: "~/plugins/moment.js" },
    { src: "~/plugins/lodash.js" },
    { src: "~/plugins/axios.js" },
    { src: "~plugins/ga.js", mode: "client" },
    // { src: "~/plugins/vue-socket.io", mode: "client" },
    { src: "~/plugins/vuex-persist", mode: "client" },
    { src: "~/plugins/currency", mode: "client" },
    { src: "~/plugins/vue-paystack", mode: "client" },
    { src: "~/plugins/globalVars.js", mode: "client" }
  ],
  /*
   ** Nuxt.js modules
   */
  devModules: ["@nuxtjs/vuetify"],
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/component-cache",
    "@nuxtjs/axios",
    "@nuxtjs/sitemap",
    "@nuxtjs/pwa",
    [
      "@nuxtjs/component-cache",
      {
        max: 10000,
        maxAge: 1000 * 60 * 60
      }
    ],
    [
      "vue-currency-filter/nuxt",
      {
        symbol: "₦",
        thousandsSeparator: ",",
        fractionCount: 2,
        fractionSeparator: ".",
        symbolPosition: "front",
        symbolSpacing: false
      }
    ]
  ],
  sitemap: {
    hostname: "https://unecem.com",
    gzip: true,
    exclude: ["/dashboard"],
    xmlNs: 'xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"',
    // routes() {
    //   return getAppRoutes();
    // },
    routes: [
      // "/",
      "/auth/login",
      "/auth/signup",
      "/about",
      "/terms",
      "/privacy"
      // {
      //   url: "/page/2",
      //   changefreq: "daily",
      //   priority: 1,
      //   lastmodISO: "2017-06-30T13:30:00.000Z"
      // }
    ],
    defaults: {
      changefreq: "daily",
      priority: 1,
      lastmod: new Date(),
      lastmodrealtime: true
    },
    generate: false
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  // 192.168.43.123
  axios: {
    // baseURL: "http://localhost:1337",
    baseURL: "https://unecem.herokuapp.com",
    retry: { retries: 0 },
    credentials: true
  },
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */

  vuetify: {
    themes: {
      theme: {
        light: {
          primary: "#0c8da0",
          secondary: "#525151",
          error: "#f44336",
          warning: "#ffc107",
          info: "#00bcd4",
          success: "#4caf50"
        },
        dark: {}
      }
    },
    // breakpoint: {
    //   thresholds: {
    //     xs: 340,
    //     sm: 575.98,
    //     md: 767.98,
    //     lg: 991.98,
    //     xl: 1199.98
    //   },
    //   scrollBarWidth: 24
    // },
    treeShake: true,
    customVariables: ["~assets/style/variables.scss"]
  },
  /*
   ** Build configuration
   */
  build: {
    extractCSS: true,
    // vendor: ["vue-paystack"],
    /*
     ** You can extend webpack config here
     */
    // transpile: ['vue-paystack'],
    extend(config, ctx) {
      if (ctx.isServer) {
        config.externals = [
          nodeExternals({
            whitelist: [/^vue-paystack/]
          })
        ];
      }
    },
    extend(config, ctx) {}
  }
};
