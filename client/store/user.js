const state = () => ({
  id: "",
  me: {},
  investments: [],
  referedUsers: [],
  orders: [],
  payout: false
});
//TODO : delete user after unsync
const mutations = {
  unsyncUser(state) {
    console.log("loggin out with ->", state);
    state.id = ""; //* Added for `auth` brevity
    state.me = {};
    state.investments = [];
    (state.orders = []), (state.payout = false);
  },
  setUser(state, data) {
    if (data.me != undefined) {
      console.log("dda:: ", data);
      state.id = data.me.id; //* Added for `auth` brevity
      state.me = data.me;
      state.investments = data.investments;
      state.orders = data.orders;
      state.referedUsers = data.referedUsers;
    }
  },
  updateUser(state, data) {
    state.me.fullName = data.fullName;
    state.me.emailAddress = data.emailAddress;
  },
  updateInvestment(state, data) {
    state.investments.push(data);
  }
};
const actions = {
  logout({ commit }) {
    this.$axios.post("/api/v1/syncs/unsync-user").then(({ data }) => {
      commit("unsyncUser");
    });
  },
  //? Synchronizes the users cart with the server if only logged in
  syncUserCart({ state, commit }) {
    if (state.me.id) {
      console.log("User found, sync started!");
      this.$axios
        .post("/api/v1/cart/sync-cart", {
          cartItems: state.cart.all
        })
        .then(({ data }) => {
          commit("cart/setCartItems", data);
        });
    } else {
      console.log("User not logged in");
    }
  },

  syncUser({ commit }) {
    this.$axios.post("/api/v1/syncs/sync-user").then(({ data }) => {
      console.log(data);
      commit("setUser", data);
    });
  },
  /**
   * object of the user to update as payload
   * the commit it to updateUser mutation for changes
   */
  updateProfile({ commit, state }, payload) {
    return new Promise((res, rej) => {
      this.$axios
        .patch("/api/v1/account/update-profile", {
          fullName: payload.fullName,
          password: payload.newPassword,
          oldPassword: payload.currentPassword,
          emailAddress: payload.emailAddress
        })
        .then(data => {
          commit("updateUser", payload);
          res(data);
        })
        .catch(err => rej(err));
    });
  },
  makeAnInvestment({ commit, state }, payload) {
    return new Promise((res, rej) => {
      this.$axios
        .post("/api/v1/investment/create", {
          amount: payload
        })
        .then(({ data }) => {
          commit("updateInvestment", data);
          res(data);
        })
        .catch(err => rej(err));
    });
  }
};
const getters = {
  getID: state => {
    return state.id;
  }
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};

//
