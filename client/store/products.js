const state = () => ({
  all: [],
  thisProduct: [], // { ...product, related: [{...product},{...product}]},
  recentlyViewed: [] // [id, id, id.. n]
});

// getters
const getters = {
  getRecentlyViewedProducts: (state, getters, rootState) => {
    return state.recentlyViewed.map(id => {
      const product = rootState.products.all.find(product => product.id === id);
      return {
        ...product
      };
    });
  }
};

// actions
const actions = {
  // Sets recently viewd product..
  // .. MAX_SAFE_LENGTH === 8
  setRecentlyViewedProduct({ state, commit }, id) {
    // state.recentlyViewed.reverse();
    if (state.recentlyViewed.length > 5) {
      // Splice or remove the first element so as to keep the MAX_SAFE_LENGTH to 8 and push the new id
      state.recentlyViewed.pop();
    }
    // Check that the product is not entered twice
    var _id = state.recentlyViewed.find(id_ => id_ === id);
    if (!_id) {
      commit("setRecentlyViewed", id);
    }
  },
  /**
   *
   *@description Gets a product for viewing
   * @param {*} param0
   * @param {*} id
   */
  getProduct({ commit, dispatch }, id) {
    if (id) {
      this.$axios
        .get("/api/v1/product/find/", { params: { product: id } })
        .then(({ data }) => {
          console.log("product", data);
          commit("setProduct", data);
          // Navigate to the product page determining its category and subcategory
          let thisProductCategory = data.category;
          let thisProductSubCategory = data.subCategory;
          // Add this product to the recently view product
          dispatch("setRecentlyViewedProduct", data.id);

          this.$router.push(
            `/${thisProductCategory}/${thisProductSubCategory}/${
              data.shortName
            }`
          );
        });
    }
  },

  getAllProducts({ commit }, category) {
    this.$axios
      .get("/api/v1/product/find-least-products", {
        category: category
      })
      .then(({ data }) => {
        console.log(data);
        commit("setProducts", data);
      });
  }
};

// mutations
const mutations = {
  /**
   * @description Set Recently Viewed Product
   * @param {*} state
   * @param {*} id
   * @returns {void}
   */
  setRecentlyViewed(state, id) {
    state.recentlyViewed.push(id);
  },
  /**
   * @description Sets one product and its related product for viewing along side with its breadcrumb
   * @param {*} state
   * @param {*} product
   */
  setProduct(state, product) {
    // debugger;
    state.thisProduct = product;
    state.thisProduct.related = product.related || [];
    //? Set the breadcrumbs of the product
    // TODO: a loop should be here
    let breadcrumbs = [
      {
        text: "Home",
        disabled: false,
        href: "/",
        nuxt: true
      },
      {
        text: _.capitalize(_.startCase(product.category)) + " ",
        disabled: false,
        href: `/${product.category}/`,
        nuxt: true
      },
      {
        text: _.capitalize(_.startCase(product.subCategory)),
        disabled: false,
        href: `/${product.category}/${product.subCategory}/`,
        nuxt: true
      },
      {
        text: _.capitalize(_.startCase(product.name)),
        disabled: true,
        href: `/${product.category}/${product.subCategory}/${product.name}`,
        nuxt: true
      }
    ];
    state.thisProduct.breadcrumbs = breadcrumbs;
  },
  setProducts(state, products) {
    state.all = products;
  },
  decrementProductInventory(state, { id }) {
    const product = state.all.find(product => product.id === id);
    // product.inventory--;
    product.quantity--;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
