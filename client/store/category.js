const state = () => {
  subheader: [];
};
const getters = {
  subheaders(state) {
    return this.subheader;
  }
};
const actions = {
  newSubheaders({ commit, state }, category) {
    this.axios
      .get("/api/v1/product/search-inputs", {})
      .then(commit("getSubheaders", category));
  }
};

const mutations = {
  getSubheaders(state, ctx) {
    state.subheader.push(ctx);
  }
};
