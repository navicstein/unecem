export const state = () => ({
  sample: 0
});

export const mutations = {
  run(state) {
    state.sample++;
  }
};
