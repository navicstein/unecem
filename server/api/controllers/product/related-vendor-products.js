module.exports = {
  friendlyName: "Related vendor products",

  description: "",

  inputs: {
    vendor: { type: "string", required: true }
  },

  exits: {},

  fn: async function(inputs) {
    var vendorProducts = await Product.find()
      .where({ vendor: inputs.vendor })
      .populateAll()
      .sort("createdAt DESC")
      .limit(6);

    // All done.
    return vendorProducts;
  }
};
