module.exports = {
  friendlyName: "Find Related Products",

  description: "Related product.",

  inputs: {
    id: { type: "string", required: true }
  },

  exits: {
    notfound: {
      responseType: "notfound",
      description: "related product not found"
    }
  },

  fn: async function(inputs) {
    var product = await Product.findOne().where({ id: inputs.id });

    if (_.isEmpty(product)) throw "notfound";

    var related = await Product.find()
      .where({
        name: { "!": product.name },
        or: [
          {
            category: { contains: product.category }
          },
          {
            category: { contains: product.subCategory }
          },
          {
            subCategory: { contains: product.category }
          },
          {
            subCategory: { contains: product.subCategory }
          }
        ]
      })
      .populateAll()
      .sort("createdAt DESC")
      .limit(10);

    // All done.
    return related;
  }
};
