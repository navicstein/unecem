module.exports = {
  friendlyName: "Find one Product",

  description: "Find product.",

  inputs: {
    product: {
      type: "string",
      required: true,
      description: "The ID or shortName of the product"
    }
    // category: {
    //   type: "string",
    //   required: false,
    //   description: "The category of the product",
    // },
  },

  exits: {
    notFound: {
      responseType: "notfound",
      description: "The product is not found"
    }
  },

  fn: async function(inputs) {
    var product = await Product.findOne()
      .where({
        or: [{ shortName: inputs.product }, { id: inputs.product }]
      })
      .populate("vendor");

    var related = await Product.find()
      .where({
        name: { "!": product.name },
        or: [
          {
            category: { contains: product.category }
          },
          {
            category: { contains: product.subCategory }
          },
          {
            subCategory: { contains: product.category }
          },
          {
            subCategory: { contains: product.subCategory }
          }
        ]
      })
      .populateAll()
      .sort("createdAt DESC")
      .limit(10);

    if (!product) throw "notFound";
    // All done.
    return { ...product, related };
  }
};
