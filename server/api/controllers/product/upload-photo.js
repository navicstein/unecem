module.exports = {
  friendlyName: "Upload photos for products",

  description: "",

  files: ["photo"],
  inputs: {
    photo: {
      description: "Upstream for an incoming file upload.",
      type: "ref",
      required: true
    }
  },

  exits: {
    tooBig: {
      description: "The file is too big.",
      responseType: "badRequest"
    }
  },

  fn: async function({ photo }) {
    var url = require("url");
    var util = require("util");
    var photo = await sails
      .uploadOne(photo, {
        maxBytes: 3000000
      })

      .intercept("E_EXCEEDS_UPLOAD_LIMIT", "tooBig")
      .intercept(
        err => new Error("The photo upload failed: " + util.inspect(err))
      );

    var imageSrc = url.resolve(
      sails.config.custom.baseUrl,
      "/api/v1/product/" + photo.id + "/photo"
    );

    var photos = await ProductPhotos.create({
      imageUploadFd: photo.fd,
      imageUploadMime: photo.type,
      product: this.req.session.productId,
      size: photo.size,
      imageSrc: imageSrc
    });


    // All done.
    return photos;
  }
};
