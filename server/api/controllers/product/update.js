module.exports = {
  friendlyName: "Update a product",

  description: "Update product.",

  inputs: {
    id: {
      type: "string",
      required: true
    },
    name: {
      type: "string",
      required: true
    },
    subCategory: {
      type: "string",
      required: true
    },
 
    shipping: {
      type: "json",
      required: true
    },
    deliveryPrice: {
      type: "number",
      required: false
    },
    description: {
      type: "string",
      required: true
    },
    specifications: {
      type: "json",
      required: true
    },
    availablity: {
      type: "string",
      required: false
    },
    // category would be dynamic
    category: {
      type: "string",
      required: true
    },
    price: {
      type: "number",
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    // ╔═╗┬─┐┌─┐┌─┐┌┬┐┌─┐  ╔═╗┬─┐┌─┐┌┬┐┬ ┬┌─┐┌┬┐
    // ║  ├┬┘├┤ ├─┤ │ ├┤   ╠═╝├┬┘│ │ │││ ││   │
    // ╚═╝┴└─└─┘┴ ┴ ┴ └─┘  ╩  ┴└─└─┘─┴┘└─┘└─┘ ┴
    var update = await Product.updateOne({ id: inputs.id }).set({
      name: inputs.name,
      shortName: _.kebabCase(inputs.name),
      description: inputs.description,
      category: inputs.category,
      specifications: inputs.specifications,
      availablity: inputs.availablity,
      price: Number(inputs.price),
      // vendor: this.req.me.id,
      subCategory: inputs.subCategory,
      shipping: Number(inputs.shipping),
      deliveryPrice: Number(inputs.deliveryPrice)
    });
    // .intercept("E_UNIQUE", "productAlreadyExist")
    // .fetch();

    this.req.session.productId = update.id;

    // All done.
    return update;
  }
};
