module.exports = {
  friendlyName: "Least products",

  description: "Find 10 Nestest products  in the dataBase",

  inputs: {},

  exits: {},

  fn: async function(inputs) {
    var products = await Product.find()
      .limit(10)
      .populate("photos");
    // All done.
    return products;
  }
};
