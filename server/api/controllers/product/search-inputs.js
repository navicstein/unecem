module.exports = {
  friendlyName: "Search inputs",

  description: "",

  inputs: {
    search: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function({ search }) {
    var products = await Product.find()
      .where({
        or: [
          {
            id: { contains: search }
          },
          // { slug: { contains: search } },
          {
            category: { contains: search }
          },
          {
            subCategory: { contains: search }
          },
          {
            name: { contains: search }
          },
          // {
          //   price: { contains: search }
          // }
        ]
      })
      .populateAll()
      .limit(10)
      .skip(0);
    // All done.
    return products;
  }
};
