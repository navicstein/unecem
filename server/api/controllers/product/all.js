module.exports = {
  friendlyName: "All Product",

  description: "Get All products.",

  inputs: {
    category: {
      type: "string",
      required: false,
      description: "The category of the products"
    },
    limit: {
      type: "number",
      required: false,
      description: "The limit of the or all products"
    },
    skip: {
      type: "number",
      required: false,
      description: "The amount of queries to skip"
    }
  },

  exits: {
    notFound: {
      responseType: "notfound",
      description: "Sorry, the product is not found"
    }
  },

  fn: async function(inputs) {
    // sort by category
    sails.log.warn(inputs.limit);

    if (inputs.category) {
      return await Product.find()
        .where({
          or: [
            { subCategory: { contains: inputs.category } },
            { category: { contains: inputs.category } }
          ]
        })
        .populateAll()
        .sort("createdAt DESC")
        .limit(inputs.limit)
        .skip(inputs.skip);
    } else {
      var product = await Product.find()
        .populateAll()
        .sort("createdAt DESC")
        .limit(inputs.limit)
        .skip(inputs.skip);
      return product;
    }
  }
};
