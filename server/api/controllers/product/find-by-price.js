module.exports = {
  friendlyName: "Find by price",

  description: "",

  inputs: {
    category: {
      type: "string",
      required: false,
      description: "The category of the products"
    },
    minPrice: {
      type: "number",
      required: true
    },
    maxPrice: {
      type: "number",
      required: true
    }
  },

  exits: {},
  fn: async function(inputs) {
    // All done.
    if (inputs.category != "all" || inputs.category != "All") {
      return await Product.find({
        price: { ">=": inputs.minPrice, "<=": inputs.maxPrice },
        or: [
          { subCategory: { contains: inputs.category } },
          { category: { contains: inputs.category } }
        ]
      }).populateAll();
    } else {
      var allProd = await Product.find({
        or: [
          {
            price: { ">": inputs.minPrice, "<": inputs.maxPrice }
          }
        ]
      }).populateAll();
      return allProd;
    }
  }
};
