module.exports = {
  friendlyName: "Create Product",

  description: "Create product.",

  inputs: {
    name: {
      type: "string",
      required: true
    },
    subCategory: {
      type: "string",
      required: true
    },
  
    shipping: {
      type: "json",
      required: true
    },
    deliveryPrice: {
      type: "number",
      required: false
    },
    description: {
      type: "string",
      required: true
    },
    specifications: {
      type: "json",
      required: true
    },
    availablity: {
      type: "string",
      required: false
    },
    // category would be dynamic
    category: {
      type: "string",
      required: true
    },
    price: {
      type: "number",
      required: true
    }
  },

  exits: {
    productAlreadyExist: {
      statusCode: 409,
      description: "This product already exist"
    }
  },
  fn: async function(inputs) {
    console.log(inputs);

    var isProductExisting = await Product.findOne({ name: inputs.name });

    if (isProductExisting) throw "productAlreadyExist";

    // ╔═╗┬─┐┌─┐┌─┐┌┬┐┌─┐  ╔═╗┬─┐┌─┐┌┬┐┬ ┬┌─┐┌┬┐
    // ║  ├┬┘├┤ ├─┤ │ ├┤   ╠═╝├┬┘│ │ │││ ││   │
    // ╚═╝┴└─└─┘┴ ┴ ┴ └─┘  ╩  ┴└─└─┘─┴┘└─┘└─┘ ┴

    var newProduct = await Product.create(
      Object.assign({
        name: inputs.name,
        shortName: _.snakeCase(inputs.name),
        description: inputs.description,
        category: inputs.category,
        specifications: inputs.specifications,
        availablity: inputs.availablity,
        price: Number(inputs.price),
        vendor: this.req.me.id,
        subCategory: inputs.subCategory,
        shipping: Number(inputs.shipping),
        deliveryPrice: Number(inputs.deliveryPrice)
      })
    )
      .intercept("E_UNIQUE", "productAlreadyExist")
      .fetch();

    this.req.session.productId = newProduct.id;

    // All done.
    return newProduct;
  }
};
