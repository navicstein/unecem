module.exports = {
  friendlyName: "search Toggle",

  description: "Find tag.",

  inputs: {
    category: {
      type: Array("string"),
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    var category = inputs.category;
    sails.log.warn(category);
    var products = await Product.find()
      .where({
        or: [
          {
            category: { in: category }
          },
          {
            subCategory: { in: category }
          }
        ]
      })
      .populateAll()
      .limit(10)
      .skip(0);

    // All done.
    return products;
  }
};
