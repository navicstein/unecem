module.exports = {
  friendlyName: "Orders",

  description: "Orders user.",

  inputs: {},

  exits: {},

  fn: async function(inputs) {
    // All done.
    return await Orders.find()
      .where({ user: this.req.me.id })
      .populateAll();
  }
};
