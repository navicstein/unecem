module.exports = {
  friendlyName: "Get",

  description: "Get user.",

  inputs: {
    // id: {
    //   type: "string",
    //   required: true
    // }
  },

  exits: {},

  fn: async function(inputs) {
    // All done.
    var user = await User.findOne().where({ id: this.req.me.id });
    return { user: this.req.me, userID: user.id } || {};
  }
};
