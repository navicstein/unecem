module.exports = {
  friendlyName: "Get",

  description: "Get comment.",

  inputs: {
    product: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    var comments = await Comment.find({
      product: inputs.product
    }).populate("user");
    // All done.
    return comments;
  }
};
