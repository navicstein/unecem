module.exports = {
  friendlyName: "Delete",

  description: "Delete comment.",

  inputs: {
    product: {
      type: "string",
      required: true,
    },
  },

  exits: {},

  fn: async function (inputs) {
    await Comment.destroyOne({ id: inputs.id });
    // All done.
    return;
  },
};
