module.exports = {
  friendlyName: "Create",

  description: "Create comment.",

  inputs: {
    comment: {
      type: "string",
      required: true
    },
    product: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    var comment = await Comment.create({
      user: this.req.session.userId,
      comment: inputs.comment,
      product: inputs.product
    }).fetch();

    comment = await Comment.findOne({ id: comment.id }).populate("user");
    // All done.
    return comment;
  }
};
