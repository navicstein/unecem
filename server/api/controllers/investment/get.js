var moment = require("moment");
module.exports = {
  friendlyName: "Get",

  description: "Get invest.",

  inputs: {},

  exits: {},

  fn: async function(inputs) {
    // sails.log.warn(this.req.me.fullName);
    var allInvest = await Investment.find({
      user: this.req.me.id,
      checkedOut: false,
      requestedPayOut: false
    });
    var checkIfPaymentIsDue = await Investment.update({
      user: this.req.me.id,
      createdAt: {
        "<": moment(allInvest.createdAt)
          .subtract(93, "days")
          .valueOf()
      }
    })
      .fetch()
      .set({
        dueForCheckout: true
      });
    var retunedInvest = await Investment.find({
      user: this.req.me.id,
      checkedOut: false,
      requestedPayOut: false
    }).populate("user");

    // await Investment.destroy({
    //   user: this.req.me.id
    // });
    // var timeLeft;
    // var t = Object.entries(retunedInvest).forEach((val, ind) => {
    //   // console.log(retunedInvest.push({t:'g'}));
    //   timeLeft = val.push("ggo");
    //   // return val[ind].push({
    //   //   timeLeft: moment(new Date(val.createdAt), "YYYYMMDD").fromNow()
    // });
    //   console.log(ind)
    // });

    // sails.log.warn(retunedInvest);

    return { invest: retunedInvest };
  }
};
