module.exports = {
  friendlyName: "Creates",

  description: "Creates investment.",

  inputs: {
    amount: { type: "number", required: true },
    dueForCheckout: { type: "boolean", defaultsTo: false },
    checkedOut: { type: "boolean", defaultsTo: false }
  },

  exits: {
    invalidAmount: {
      statusCode: 406
    }
  },

  fn: async function({ amount }) {
    if (amount < 50000) {
      throw "invalidAmount";
    }
    function percentage(_rate, _amount) {
      // var rate = 2.3;
      return Math.floor((_rate * _amount) / 100);
    }

    var owner = await Referal.findOne({ referedUser: this.req.me.id });
    if (owner) {
      owner = await User.findOne({ id: owner.referedBy });

      //  IF ITS ONCE
      var alreadyInvested = await Investment.find({
        user: this.req.me.id,
        referedOwner: owner.id
      }).limit(1);
      if (_.size(alreadyInvested) > 0) {
        sails.log.warn(`Not crediting the \`referalBonus\` of "${owner.id}"
         beacuse you have gotten a bouns from the referd`);
      } else {
        // ELSE
        console.log("::", owner.referalBonus);
        await User.updateOne({ id: owner.id }).set({
          referalBonus: owner.referalBonus + percentage(2.3, amount)
        });
      }

      var investment = await Investment.create({
        user: this.req.me.id,
        amount: amount,
        profit: percentage(5, amount),
        payout: amount + percentage(5, amount),
        referedOwner: owner.id
      }).fetch();
      sails.log("amount", investment);
      return investment;
    } else {
      var investment = await Investment.create({
        user: this.req.me.id,
        amount: amount,
        profit: percentage(5, amount),
        payout: amount + percentage(5, amount)
      }).fetch();
      sails.log("amount", investment);
      return investment;
    }

    // All done.
  }
};
