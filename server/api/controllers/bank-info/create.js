module.exports = {
  friendlyName: "Create",

  description: "Create bank info.",

  inputs: {
    fullName: { type: "string", required: true, maxLength: 120 },

    bankName: { type: "string", required: true },

    accountNumber: { type: "string", required: true }
  },

  exits: {},

  fn: async function(inputs) {
    var getBank = await BankInfo.findOne({
      user: this.req.me.id
    });
    if (getBank) {
      console.log(getBank);
      var bank = await BankInfo.updateOne({
        user: this.req.me.id
      }).set({
        fullName: inputs.fullName,
        bankName: inputs.bankName,
        accountNumber: inputs.accountNumber
      });
    } else {
      var bank = await BankInfo.create({
        user: this.req.me.id,
        fullName: inputs.fullName,
        bankName: inputs.bankName,
        accountNumber: inputs.accountNumber
      });
    }
    await User.updateOne({
      id: this.req.me.id
    }).set({
      bankInfo: true
    });
    // All done.
    return;
  }
};
