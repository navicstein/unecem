module.exports = {
  friendlyName: "Get",

  description: "Get bank info.",

  inputs: {},

  exits: {},

  fn: async function(inputs) {
    var bank = await BankInfo.find({
      user: this.req.me.id
    });
    // console.log(bank);
    // console.log("bank", bank);
    // var test = await User.updateOne({
    //   id: this.req.me.id
    // }).set({
    //   bankInfo: false
    // });

    // console.log(test);
    // All done.
    return bank[0];
  }
};
