module.exports = {
  friendlyName: "Delete image",

  description: "",

  inputs: {
    id: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    // pull the latest ones first
    var image = await SiteImage.findOne({ id: inputs.id });

    // fs.unlinkSync(image.imageUploadFd);
    // await SiteImage.destroy({ id: image.id });
    if (image) {
      try {
        await sails.rm(image.imageUploadFd);
      } catch {
        //
      }
      await SiteImage.destroy({ id: inputs.id });
    }
    // All done.
    return;
  }
};
