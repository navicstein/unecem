module.exports = {
  friendlyName: "Upload photos",

  description: "",

  files: ["photo"],
  inputs: {
    photo: {
      description: "Upstream for an incoming file upload.",
      type: "ref",
      required: true
    },
    type: {
      type: "string",
      required: true
    }
  },

  exits: {
    tooBig: {
      description: "The file is too big.",
      responseType: "badRequest"
    }
  },

  fn: async function({ photo, type }) {
    var url = require("url"),
      util = require("util");
    // pull the latest ones first
    // var images = await SiteImage.find({});
    // images.forEach(async image => {
    //   try {
    //     fs.unlinkSync(image.imageUploadFd);
    //   } catch {}
    //   await SiteImage.destroy({ id: image.id });
    // });

    var photo = await sails
      .uploadOne(photo, {
        maxBytes: 3000000,
        //
      
      })
      .intercept("E_EXCEEDS_UPLOAD_LIMIT", "tooBig")
      .intercept(
        err => new Error("The photo upload failed: " + util.inspect(err))
      );

    var upl = await SiteImage.create({
      imageUploadFd: photo.fd,
      imageUploadMime: photo.type,
      size: photo.size,
      type: type
    }).fetch();
    console.log(upl);
    // All done.
    return upl;
  }
};
