module.exports = {
  friendlyName: "Get images",

  description: "",

  inputs: {
    type: { required: true, type: "string" }
  },

  exits: {},

  fn: async function(inputs) {
    var images = await SiteImage.find()
      .where({ type: inputs.type })
      .sort("createdAt DESC");
    // All done.
    return images;
  }
};
