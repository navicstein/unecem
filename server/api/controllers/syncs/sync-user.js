module.exports = {
  friendlyName: "Sync user",

  description: "",

  inputs: {},

  exits: {},

  fn: async function(inputs) {
    // All done.
    // var user = await user.find().populate("referal");
    if (this.req.me) {
      var me = await User.findOne(this.req.me.id);
      var investments = await Investment.find({ user: this.req.me.id });
      var cart = await Cart.find({ user: me.id });
      var referedUsers = await Referal.find().populate("referedUser");
      let response = { me, cart, investments, referedUsers };

      return response;
    }
    // throw "noUser";
  }
};
