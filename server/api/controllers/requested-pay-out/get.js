module.exports = {
  friendlyName: "Get",

  description: "Get requested pay out.",

  inputs: {},

  exits: {},

  fn: async function(inputs) {
    var allNewReq = await RequestedPayOut.find().populate("user");
    // await RequestedPayOut.destroy({
    //   fullName:'bb' || 'ea'
    // });
    sails.log.warn(allNewReq);
    // All done.
    return allNewReq;
  }
};
