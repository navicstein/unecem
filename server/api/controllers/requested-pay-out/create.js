module.exports = {
  friendlyName: "Create",

  description: "Create reqested pay out.",

  inputs: {
    fullName: { type: "string", required: true, maxLength: 120 },
    bankName: { type: "string", required: true },

    accountNumber: { type: "string", required: true },
    amount: {
      type: "number",
      required: true
    },
    refAmount: {
      type: "number"
    }
  },

  exits: {
    reqTrue: {
      statusCode: 406,
      discription: "Payout request has already been made"
    }
  },

  fn: async function(inputs) {
    var checkIfPaymentIsDue = await Investment.update({
      id: inputs.id
    }).set({
      requestedPayOut: true
    });
    var newReq = await RequestedPayOut.create({
      fullName: inputs.fullName,
      bankName: inputs.bankName,
      amount: inputs.amount,
      refAmount: inputs.refAmount,
      accountNumber: inputs.accountNumber,
      user: this.req.me.id
    }).fetch();
    // All done.
    return newReq;
  }
};
