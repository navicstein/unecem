module.exports = {
  friendlyName: "Patch",

  description: "Patch orders.",

  inputs: {
    id: {
      type: "string",
      required: true
    },
    isPaid: {
      type: "boolean",
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    await Order.updateOne({
      id: inputs.id
    }).set({
      isPaid: inputs.isPaid
    });

    // All done.
    return;
  }
};
