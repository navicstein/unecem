module.exports = {
  friendlyName: "Get all order",

  description: "",

  inputs: {
    isPaid: {
      type: "boolean",
      required: true
    }
  },

  exits: {},

  fn: async function({ isPaid }) {
    var orders = await Order.find({
      isPaid: isPaid
    }).populateAll();


    // All done.
    return orders;
  }
};
