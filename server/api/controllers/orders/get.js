module.exports = {
  friendlyName: "Get",

  description: "Get orders.",

  inputs: {
    
  },

  exits: {},

  fn: async function(inputs) {
    var orders = await Order.find({ user: this.req.me.id });
    // All done.
    return orders;
  }
};
