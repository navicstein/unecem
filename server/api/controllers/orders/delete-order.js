module.exports = {
  friendlyName: "Delete order",

  description: "",

  inputs: {
    id: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    await Order.destroy({ id: inputs.id });
    // All done.
    return;
  }
};
