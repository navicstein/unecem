module.exports = {
  friendlyName: "Inbox",

  description: "Get contact us.",

  inputs: {},

  exits: {},

  fn: async function(inputs) {
    var allMails = await ContactUs.find();
    // All done.
    return allMails;
  }
};
