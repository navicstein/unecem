module.exports = {
  friendlyName: "Delete user message",

  description: "",

  inputs: {
    id: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function({ id }) {
    await ContactUs.destroy({
      id: id
    });
    // All done.
    return;
  }
};
