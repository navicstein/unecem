module.exports = {
  friendlyName: "send uncem a message",

  description: "Create a message for unecem .",

  inputs: {
    emailAddress: { type: "string", required: true },
    fullName: { type: "string", required: true },
    topic: { type: "string", required: true },
    message: { type: "string", required: true }
  },

  exits: {},

  fn: async function(inputs, exit) {
    console.log(inputs);
    var contactUS = await ContactUs.create({
      emailAddress: inputs.emailAddress,
      fullName: inputs.fullName,
      topic: inputs.topic,
      message: inputs.message
    });
    // var htmlTemplate = `
    // `
    await sails.helpers.sendEmail.with({
      to: "unecemglobal@gmail.com",
      fullName: inputs.fullName,
      from: inputs.emailAddress,
      subject: inputs.topic,
      content: inputs.message
    });
    // All done.
    return "ok";
  }
};
