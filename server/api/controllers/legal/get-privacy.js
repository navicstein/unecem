module.exports = {
  friendlyName: "Get privacy",

  description: "",

  inputs: {},

  exits: {},

  fn: async function(inputs) {
    var privacy = await Privacy.find();

    // All done.
    return privacy[0];
  }
};
