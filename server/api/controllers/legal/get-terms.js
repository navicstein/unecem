module.exports = {
  friendlyName: "Get terms",

  description: "",

  inputs: {},

  exits: {},

  fn: async function(inputs) {
    var terms = await Terms.find();
    // All done.
    return terms[0];
  }
};
