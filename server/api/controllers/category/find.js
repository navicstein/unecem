module.exports = {
  friendlyName: "Find parent",

  description: "Find tag.",

  inputs: {
    parent: {
      type: "string"
    }
  },

  exits: {},

  fn: async function(inputs) {
    // var category = inputs.category;
    // var products = await Product.find()
    //   .where({
    //     or: [
    //       {
    //         category: { contains: category }
    //       },
    //       {
    //         subCategory: { contains: category }
    //       }
    //     ]
    //   })
    //   .populateAll();
    if (inputs.parent) {
      var category = await Category.find()
        .where({
          parent: { contains: inputs.parent }
        })
        .sort("parent ASC");
      // All done.
      return category;
    } else {
      var category = await Category.find();
      return category;
    }
  }
};
