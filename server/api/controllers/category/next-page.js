module.exports = {
  friendlyName: "Next page",

  description: "",

  inputs: {
    page: {
      type: "number",
      required: true
    },
    category: {
      type: "string",
      required: false,
      description: "The category of the products"
    },
    minPrice: {
      type: "number",
      required: true
    },
    maxPrice: {
      type: "number",
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    if (inputs.category != "all" || inputs.category != "All") {
      // console.log(inputs.page - 1);
      return await Product.find({
        price: { ">=": inputs.minPrice, "<=": inputs.maxPrice },
        or: [
          { subCategory: { contains: inputs.category } },
          { category: { contains: inputs.category } }
        ]
      })
        .paginate({
          page: inputs.page - 1,
          limit: 20
        })
        .populateAll();
    } else {
      return await Product.find({
        price: { ">=": inputs.minPrice, "<=": inputs.maxPrice }
      })
        .paginate({
          page: inputs.page - 1,
          limit: 20
        })
        .populateAll();
    }
  }
};
