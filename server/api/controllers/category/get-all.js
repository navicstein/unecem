module.exports = {
  friendlyName: "Get all category and subcategory",

  description: "",

  inputs: {
    // category: {
    //   type: "string",
    //   required: false
    // },
    // subcategory: {
    //   type: "string",
    //   required: false
    // }
  },

  exits: {},

  // no database call needed
  fn: async function(inputs) {
    var category = Category.find().sort("parent ASC");

    // All done.
    return category;
  }
};
