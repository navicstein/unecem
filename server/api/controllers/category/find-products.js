module.exports = {
  friendlyName: "Find products",

  description: "",

  inputs: {
    category: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    var category = inputs.category;
    var products = await Product.find()
      .where({
        or: [
          {
            category: { contains: category }
          },
          {
            subCategory: { contains: category }
          }
        ]
      })
      .populateAll();

    // All done.
    return products;
  }
};
