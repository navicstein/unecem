module.exports = {
  friendlyName: "Signup",

  description: "Sign up for a new user account.",

  extendedDescription: `This creates a new user record in the database, signs in the requesting user agent
by modifying its [session](https://sailsjs.com/documentation/concepts/sessions), and
(if emailing with Mailgun is enabled) sends an account verification email.

If a verification email is sent, the new user's account is put in an "unconfirmed" state
until they confirm they are using a legitimate email address (by clicking the link in
the account verification message.)`,

  inputs: {
    emailAddress: {
      required: true,
      type: "string",
      isEmail: true,
      description: "The email address for the new account, e.g. m@example.com.",
      extendedDescription: "Must be a valid email address."
    },

    password: {
      required: true,
      type: "string",
      maxLength: 200,
      example: "passwordlol",
      description: "The unencrypted password to use for the new account."
    },

    ref: {
      type: "string",
      required: false
    },

    fullName: {
      required: true,
      type: "string",
      example: "Frida Kahlo de Rivera",
      description: "The user's full name."
    },
    // isVendor: {
    //   required: false,
    //   type: "boolean"
    // },
    isSuperAdmin: {
      required: false,
      type: "boolean"
    }
  },

  exits: {
    success: {
      description: "New user account was created successfully."
    },

    invalid: {
      responseType: "badRequest",
      description:
        "The provided fullName, password and/or email address are invalid.",
      extendedDescription:
        "If this request was sent from a graphical user interface, the request " +
        "parameters should have been validated/coerced _before_ they were sent."
    },

    emailAlreadyInUse: {
      statusCode: 409,
      description: "The provided email address is already in use."
    }
  },

  fn: async function(inputs) {
    var newEmailAddress = inputs.emailAddress.toLowerCase();
    // ██████╗ ███████╗███████╗███████╗██████╗  █████╗ ██╗     ███████╗
    // ██╔══██╗██╔════╝██╔════╝██╔════╝██╔══██╗██╔══██╗██║     ██╔════╝
    // ██████╔╝█████╗  █████╗  █████╗  ██████╔╝███████║██║     ███████╗
    // ██╔══██╗██╔══╝  ██╔══╝  ██╔══╝  ██╔══██╗██╔══██║██║     ╚════██║
    // ██║  ██║███████╗██║     ███████╗██║  ██║██║  ██║███████╗███████║
    // ╚═╝  ╚═╝╚══════╝╚═╝     ╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚══════╝

    // ╦  ┌─┐┌─┐┬┌─  ┬ ┬┌─┐  ┌┬┐┬ ┬┌─┐  ┬─┐┌─┐┌─┐  ┌─┐┬┬─┐┌─┐┌┬┐
    // ║  │ ││ │├┴┐  │ │├─┘   │ ├─┤├┤   ├┬┘├┤ ├┤   ├┤ │├┬┘└─┐ │
    // ╩═╝└─┘└─┘┴ ┴  └─┘┴     ┴ ┴ ┴└─┘  ┴└─└─┘└    └  ┴┴└─└─┘ ┴
    inputs.ref = _.trim(inputs.ref);
    var ref = await User.findOne().where({ id: inputs.ref });
    if (!ref) {
      sails.log.warn(
        `The user that has this referal link \`${inputs.ref}\` is not found`
      );
    }

    // Build up data for the new user record and save it to the database.
    // (Also use `fetch` to retrieve the new ID so that we can use it below.)
    var findUser = await User.findOne({ emailAddress: inputs.emailAddress });
    if (!findUser) {
      var newUserRecord = await User.create({
        emailAddress: newEmailAddress,
        password: await sails.helpers.passwords.hashPassword(inputs.password),
        fullName: inputs.fullName,
        tosAcceptedByIp: this.req.ip,
        isSuperAdmin: inputs.isSuperAdmin ? true : false,
        referedBy: ref ? ref.id : ""
      })
        // .intercept("E_UNIQUE", "emailAlreadyInUse")
        // .intercept({ name: "UsageError" }, "invalid")
        .fetch();
      // console.log("user :::", newUserRecord);
      // Store the user's new id in their session.
      this.req.session.userId = newUserRecord.id;
      if (ref) {
        var y = await Referal.create({
          referedBy: ref.id,
          referedUser: newUserRecord.id
        }).fetch();
        await User.updateOne({ id: ref.id }).set({
          totalReferals: ref.totalReferals + 1
        });
        // sails.log.warn(y);
      }

      if (sails.config.custom.verifyEmailAddresses) {
        // Send "confirm account" email
        await sails.helpers.sendTemplateEmail.with({
          to: newEmailAddress,
          subject: "Please confirm your account",
          template: "email-verify-account",
          templateData: {
            fullName: inputs.fullName,
            token: newUserRecord.emailProofToken
          }
        });
      } else {
        sails.log.info(
          "Skipping new account email verification... (since `verifyEmailAddresses` is disabled)"
        );
      }
      // return newUserRecord;
      // return {
      //   fullName: newUserRecord.fullName,
      //   emailAddress: newUserRecord.emailAddress,
      //   emailStatus: newUserRecord.emailStatus,
      //   createdAt: newUserRecord.createdAt,
      //   updatedAt: newUserRecord.updatedAt,
      //   lastSeenAt: newUserRecord.lastSeenAt,
      //   referedBy: newUserRecord.referedBy,
      //   isSuperAdmin: newUserRecord.isSuperAdmin,

      //   referals: newUserRecord.referals,
      //   referalBonus: newUserRecord.referalBonus,
      //   totalReferals: newUserRecord.totalReferals,
      //   reviews: newUserRecord.reviews,
      //   hasInvested: newUserRecord.hasInvested,
      //   viewedProductCategory: newUserRecord.viewedProductCategory
      // };
    } else {
      throw "emailAlreadyInUse";
    }
  }
};
