module.exports = {
  friendlyName: "Auth provider",
  description: "",
  inputs: {},
  exits: {},
  fn: async function(inputs) {
    var GoogleAPIsOAuth2v2 = require("machinepack-googleapisoauth2v2");
    // Getting access token using authorization token that Google provided you with
    GoogleAPIsOAuth2v2.getAccessToken({
      clientId:
        "349595948454-75plktnvbj0gedsiobpit7vj721v1nh8.apps.googleusercontent.com",
      clientSecret: "A9O2BbbAYDLrBjWpRho03e3H",
      redirectUrl: "http://localhost:1337/user/google/login",
      code: "someSuperCodeYouGotFromGoogle"
    }).exec({
      // An unexpected error occurred.
      error: function(err) {},
      // Triggers if some of input variables are wrong.
      invalidRequest: function() {},
      // Triggers if code is wrong.
      invalidToken: function() {},
      // OK.
      success: function() {}
    });
    // All done.
    return;
  }
};
