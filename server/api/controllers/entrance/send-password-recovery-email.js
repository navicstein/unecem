module.exports = {
  friendlyName: "Send email",

  description: "Sails helper to send emails",

  inputs: {
    to: {
      type: "string",
      required: true
    }
    // subject: {
    //   type: "string",
    //   required: true
    // },
    // content: {
    //   type: "string",
    //   // required: true
    // }
  },

  exits: {
    success: {
      description: "All done."
    }
  },

  fn: async function(inputs) {
    var nodemailer = require("nodemailer");
    var transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "unecemglobal@gmail.com",
        pass: "une@2019"
      }
    });
    function generateRecovryLink() {
      let text = "";
      let possible =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      for (let i = 0; i < 6; ++i) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      return text;
    }

    var data = {
      from: "unecem <no-reply@unecem.com>",
      to: inputs.to,
      subject: "password Recovry mail",
      html: `<!DOCTYPE html>
      <html lang="en"  ⚡4email>
        <head>
          <meta charset="UTF-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <meta http-equiv="X-UA-Compatible" content="ie=edge" />
          <title>Document</title>
        </head>
        <body>
          <div
            style='color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-size:14px;line-height:1.5;margin:0'
          >
            <div
              style="display:none;width:0;max-height:0;overflow:hidden;height:0;font-size:0;line-height:0;margin:0 auto"
            >
              <table style="width:100%" width="100%">
                <tbody>
                  <tr style="line-height:0">
                    <td>&nbsp;</td>
                    <td
                      align="center"
                      width="1"
                      style="text-align:center;width:1px"
                    ></td>
                    <td>&nbsp;</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <table
              class="m_-6451105117217453493wrapper-table"
              cellpadding="5"
              cellspacing="0"
              width="100%"
              border="0"
              style='border-collapse:collapse;color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-size:14px;line-height:1.5;background-color:#eee;background-repeat:no-repeat'
              bgcolor="#eeeeee"
            >
              <tbody>
                <tr style="border-color:transparent">
                  <td
                    align="center"
                    style="border-collapse:collapse;border-color:transparent"
                  >
                    <table
                      cellpadding="0"
                      cellspacing="0"
                      width="600px"
                      id="m_-6451105117217453493bodyTable"
                      border="0"
                      bgcolor="#ffffff"
                      style='border-collapse:collapse;color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-size:14px;line-height:1.5'
                    >
                      <tbody>
                        <tr style="border-color:transparent">
                          <td
                            border="0"
                            cellpadding="0"
                            cellspacing="0"
                            style="border-collapse:collapse;border-color:transparent"
                          >
                            <table
                              cellpadding="0"
                              cellspacing="0"
                              style='border-collapse:collapse;color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-size:14px;line-height:1.5;width:100%'
                              border="0"
                              width="100%"
                            >
                              <tbody>
                                <tr style="border-color:transparent">
                                  <th
                                    width="600"
                                    style="border-color:transparent;font-weight:normal;text-align:left;vertical-align:top"
                                    cellpadding="0"
                                    cellspacing="0"
                                    class="m_-6451105117217453493tc"
                                    align="left"
                                    valign="top"
                                  >
                                    <table
                                      border="0"
                                      width="100%"
                                      cellpadding="0"
                                      cellspacing="0"
                                      style='border-collapse:collapse;color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-size:14px;line-height:1.5;background-color:#009fc1'
                                      bgcolor="#009fc1"
                                    >
                                      <tbody>
                                        <tr style="border-color:transparent">
                                          <td
                                            cellpadding="0"
                                            cellspacing="0"
                                            style="border-collapse:collapse;border-color:transparent"
                                          >
                                            <table
                                              width="100%"
                                              cellpadding="0"
                                              cellspacing="0"
                                              style='border-collapse:collapse;color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-size:14px;line-height:1.5;background-color:#009fc1'
                                              bgcolor="#009fc1"
                                            >
                                              <tbody>
                                                <tr style="border-color:transparent">
                                                  <td
                                                    class="m_-6451105117217453493expander"
                                                    colspan="3"
                                                    width="100%"
                                                    height="20"
                                                    style="border-collapse:collapse;border-color:transparent"
                                                  ></td>
                                                </tr>
                                                <tr
                                                  class="m_-6451105117217453493content-row"
                                                  style="border-color:transparent"
                                                >
                                                  <td
                                                    class="m_-6451105117217453493gutter"
                                                    style="border-collapse:collapse;border-color:transparent;width:30px!important"
                                                    width="30"
                                                    height="100%"
                                                  ></td>
                                                  <td
                                                    class="m_-6451105117217453493content-cell"
                                                    width="540"
                                                    style="border-collapse:collapse;border-color:transparent;vertical-align:top"
                                                    valign="top"
                                                  >
                                                    <div
                                                      style='color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-size:14px;line-height:1.5;display:block;height:26;text-align:center;width:100%'
                                                      height="26"
                                                      align="center"
                                                      width="100%"
                                                    >
                                                     Unecem
                                                    </div>
                                                  </td>
                                                  <td
                                                    class="m_-6451105117217453493gutter"
                                                    style="border-collapse:collapse;border-color:transparent;width:30px!important"
                                                    width="30"
                                                    height="100%"
                                                  ></td>
                                                </tr>
                                                <tr style="border-color:transparent">
                                                  <td
                                                    class="m_-6451105117217453493expander"
                                                    colspan="3"
                                                    width="100%"
                                                    height="20"
                                                    style="border-collapse:collapse;border-color:transparent"
                                                  ></td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </th>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr style="border-color:transparent">
                          <td
                            border="0"
                            cellpadding="0"
                            cellspacing="0"
                            style="border-collapse:collapse;border-color:transparent"
                          >
                            <table
                              cellpadding="0"
                              cellspacing="0"
                              style='border-collapse:collapse;color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-size:14px;line-height:1.5;width:100%'
                              border="0"
                              width="100%"
                            >
                              <tbody>
                                <tr style="border-color:transparent">
                                  <th
                                    width="600"
                                    style="border-color:transparent;font-weight:normal;text-align:left;vertical-align:top"
                                    cellpadding="0"
                                    cellspacing="0"
                                    class="m_-6451105117217453493tc"
                                    align="left"
                                    valign="top"
                                  >
                                    <table
                                      border="0"
                                      width="100%"
                                      cellpadding="0"
                                      cellspacing="0"
                                      style='border-collapse:collapse;color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-size:14px;line-height:1.5;background-color:#fff'
                                      bgcolor="#ffffff"
                                    >
                                      <tbody>
                                        <tr style="border-color:transparent">
                                          <td
                                            cellpadding="0"
                                            cellspacing="0"
                                            style="border-collapse:collapse;border-color:transparent"
                                          >
                                            <table
                                              width="100%"
                                              cellpadding="0"
                                              cellspacing="0"
                                              id="m_-6451105117217453493w"
                                              style='border-collapse:collapse;color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-size:14px;line-height:1.5;background-color:#fff;font-weight:normal;margin:0'
                                              bgcolor="#ffffff"
                                            >
                                              <tbody>
                                                <tr style="border-color:transparent">
                                                  <td
                                                    class="m_-6451105117217453493expander"
                                                    colspan="3"
                                                    width="100%"
                                                    height="40"
                                                    style="border-collapse:collapse;border-color:transparent"
                                                  ></td>
                                                </tr>
                                                <tr
                                                  class="m_-6451105117217453493content-row"
                                                  style="border-color:transparent"
                                                >
                                                  <td
                                                    class="m_-6451105117217453493gutter"
                                                    style="border-collapse:collapse;border-color:transparent;width:40px!important"
                                                    width="40"
                                                    height="100%"
                                                  ></td>
                                                  <td
                                                    class="m_-6451105117217453493content-cell"
                                                    width="520"
                                                    style="border-collapse:collapse;border-color:transparent;vertical-align:top"
                                                    valign="top"
                                                  >
                                                    <p
                                                      style='font-size:inherit;line-height:inherit;margin:0 0 10px 0;color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-weight:normal;padding:0'
                                                    >
                                                      <span style="color:#009fc1"
                                                        ><span style="font-size:24px"
                                                          ><strong
                                                            >Password Recovry</strong
                                                          ></span
                                                        ></span
                                                      >
                                                    </p>
                                                    <p
                                                      style='font-size:inherit;line-height:inherit;margin:0 0 10px 0;color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-weight:normal;padding:0'
                                                    >
                                                      <span style="font-size:16px"
                                                        >Someone requested a password
                                                        reset for your account. If
                                                        this was not you, please
                                                        disregard this email.
                                                        Otherwise, simply click the
                                                        button below:</span
                                                      >
                                                    </p>
                                                    <p
                                                      style='font-size:inherit;line-height:inherit;margin:0 0 10px 0;color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-weight:normal;padding:0'
                                                    >
                                                      <span style="font-size:16px"
                                                        >This link will expaire after
                                                        10 mintes</span
                                                      >
                                                    </p>
                                                    <p
                                                      style='font-size:inherit;line-height:inherit;margin:0 0 10px 0;color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-weight:normal;padding:0'
                                                    >
                                                      <span style="font-size:16px"
                                                        >If you have any trouble, try
                                                        pasting this link in your
                                                        browser</span
                                                      >
                                                     <a href="https://unecem.com/password/new/${generateRecovryLink()}">https://unecem.com/password/new/${generateRecovryLink()}</a>
                                                    </p>
                                                  </td>
                                                  <td
                                                    class="m_-6451105117217453493gutter"
                                                    style="border-collapse:collapse;border-color:transparent;width:40px!important"
                                                    width="40"
                                                    height="100%"
                                                  ></td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </th>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      
                       
                        <tr style="border-color:transparent">
                          <td
                            border="0"
                            cellpadding="0"
                            cellspacing="0"
                            style="border-collapse:collapse;border-color:transparent"
                          >
                            <table
                              cellpadding="0"
                              cellspacing="0"
                              style='border-collapse:collapse;color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-size:14px;line-height:1.5;width:100%'
                              border="0"
                              width="100%"
                            >
                              <tbody>
                                <tr style="border-color:transparent">
                                  <th
                                    width="600"
                                    style="border-color:transparent;font-weight:normal;text-align:left;vertical-align:top"
                                    cellpadding="0"
                                    cellspacing="0"
                                    class="m_-6451105117217453493tc"
                                    align="left"
                                    valign="top"
                                  >
                                    <table
                                      border="0"
                                      width="100%"
                                      cellpadding="0"
                                      cellspacing="0"
                                      style='border-collapse:collapse;color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-size:14px;line-height:1.5;background-color:#fff'
                                      bgcolor="#ffffff"
                                    >
                                      <tbody>
                                        <tr style="border-color:transparent">
                                          <td
                                            cellpadding="0"
                                            cellspacing="0"
                                            style="border-collapse:collapse;border-color:transparent"
                                          >
                                            <div
                                              class="m_-6451105117217453493block-divider"
                                              style='color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-size:14px;line-height:1.5;padding-bottom:15px;padding-left:40px;padding-right:40px;padding-top:20px'
                                            >
                                              <hr
                                                style="margin:0;border-bottom:0;border-left:0;border-right:0;border-top-color:#dadada;border-top-style:solid;border-top-width:1px"
                                              />
                                            </div>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </th>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr style="border-color:transparent">
                          <td
                            border="0"
                            cellpadding="0"
                            cellspacing="0"
                            style="border-collapse:collapse;border-color:transparent"
                          >
                            <table
                              cellpadding="0"
                              cellspacing="0"
                              style='border-collapse:collapse;color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-size:14px;line-height:1.5;width:100%'
                              border="0"
                              width="100%"
                            >
                              <tbody>
                                <tr style="border-color:transparent">
                                  <th
                                    width="600"
                                    style="border-color:transparent;font-weight:normal;text-align:left;vertical-align:top"
                                    cellpadding="0"
                                    cellspacing="0"
                                    class="m_-6451105117217453493tc"
                                    align="left"
                                    valign="top"
                                  >
                                    <table
                                      border="0"
                                      width="100%"
                                      cellpadding="0"
                                      cellspacing="0"
                                      style='border-collapse:collapse;color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-size:14px;line-height:1.5;background-color:#fff'
                                      bgcolor="#ffffff"
                                    >
                                      <tbody>
                                        <tr style="border-color:transparent">
                                          <td
                                            cellpadding="0"
                                            cellspacing="0"
                                            style="border-collapse:collapse;border-color:transparent"
                                          >
                                            <table
                                              width="100%"
                                              cellpadding="0"
                                              cellspacing="0"
                                              id="m_-6451105117217453493w"
                                              style='border-collapse:collapse;color:#333;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-size:14px;line-height:1.5;font-weight:normal;margin:0'
                                            >
                                              <tbody>
                                                <tr style="border-color:transparent">
                                                  <td
                                                    class="m_-6451105117217453493expander"
                                                    colspan="3"
                                                    width="100%"
                                                    height="15"
                                                    style="border-collapse:collapse;border-color:transparent"
                                                  ></td>
                                                </tr>
                                                <tr
                                                  class="m_-6451105117217453493content-row"
                                                  style="border-color:transparent"
                                                >
                                                  <td
                                                    class="m_-6451105117217453493gutter"
                                                    style="border-collapse:collapse;border-color:transparent;width:30px!important"
                                                    width="30"
                                                    height="100%"
                                                  ></td>
                                                  <td
                                                    class="m_-6451105117217453493content-cell"
                                                    width="540"
                                                    style="border-collapse:collapse;border-color:transparent;vertical-align:top"
                                                    valign="top"
                                                  >
                                                    <p
                                                      style='font-size:inherit;line-height:inherit;margin:0 0 10px 0;color:inherit;font-family:"Segoe UI",Segoe,"Avenir Next","Open Sans",sans-serif;font-weight:normal;padding:0;text-align:center'
                                                      align="center"
                                                    >
                                                      <span style="font-size:16px"
                                                        >Best wishes, Unecem
                                                        Team&nbsp;<br
                                                      /></span>
                                                    </p>
                                                  </td>
                                                  <td
                                                    class="m_-6451105117217453493gutter"
                                                    style="border-collapse:collapse;border-color:transparent;width:30px!important"
                                                    width="30"
                                                    height="100%"
                                                  ></td>
                                                </tr>
                                                <tr style="border-color:transparent">
                                                  <td
                                                    class="m_-6451105117217453493expander"
                                                    colspan="3"
                                                    width="100%"
                                                    height="10"
                                                    style="border-collapse:collapse;border-color:transparent"
                                                  ></td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </th>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        
            <div class="adL"></div>
          </div>
        </body>
      </html>
      `
    };

    sails.log.debug(data);

    transporter.sendMail(data, function(error, info) {
      if (error) {
        sails.log.error(error);
      } else {
        sails.log.info("Email sent: " + info.response);
      }
    });
  }
};
