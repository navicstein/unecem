module.exports = {
  friendlyName: "Logout",

  description: "Logout entrance.",

  inputs: {},

  exits: {},

  fn: async function(inputs) {
    this.res.clearCookie();
    delete this.req.me;
    delete this.req.session.userId;
    // All done.
    return;
  }
};
