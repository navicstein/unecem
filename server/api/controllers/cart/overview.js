module.exports = {
  friendlyName: "Overview",

  description: "Overview cart.",

  inputs: {
    // productId: {
    //   type: ["string"],
    //   required: true,
    //   description: "The list of ids of cart to display"
    // }
  },

  exits: {},

  fn: async function(inputs) {
    console.log("cart ids", inputs.productId);
    var cartId = await Cart.find({
      user: this.req.me.id
    });
    var cartItems = await Product.find()
      .where({
        id: { in: _.omit(cartId, ['id', 'user']) }
      })
      .populate("photos")
      .populate("vendor");
    // All done.
    return cartItems;
  }
};
