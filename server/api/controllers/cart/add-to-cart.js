module.exports = {
  friendlyName: "Add to cart",

  description: "Add Products to cart",

  inputs: {
    productId: {
      type: "string"
    }
  },

  exits: {},

  fn: async function(inputs) {
    var addedCartItem = await Cart.create({
      user: this.req.me.id,
      productId: inputs.productId
    }).fetch();
    // All done.
    return addedCartItem;
  }
};
