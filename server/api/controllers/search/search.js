module.exports = {
  friendlyName: "Search",

  description: "Search search.",

  inputs: {
    search: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function({ search }) {
    var result;
    result = await Product.find()
      .where({
        or: [
          { id: search },
          { slug: { contains: search } },
          { name: { contains: search } },
          { category: { contains: search } },
          { subCategory: { contains: search } }
        ]
      })
      .populateAll();

    // if (_.isEmpty(result)) {
    //   result = await User.find().where({
    //     isVendor: true,
    //     or: [
    //       { id: { contains: search } },
    //       { url: { contains: search } },
    //       { fullName: { contains: search } }
    //     ]
    //   });
    // }
    // All done.
    return result;
  }
};
