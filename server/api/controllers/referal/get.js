module.exports = {
  friendlyName: "Get",

  description: "Get referal.",

  inputs: {
    // id: {
    //   type: "string",
    //   required: true
    // }
  },

  exits: {},

  fn: async function(inputs) {
    var referals = await Referal.find().where({
      referedBy: this.req.me.id
    }).populate('referedUser');

    ;
    // All done.
    return referals;
  }
};
