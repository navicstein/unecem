module.exports = {
  friendlyName: "Update Privacy",

  description: "create Privacy policy",

  inputs: {
    content: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    var Oldcontent = await Privacy.find({});
    if (Oldcontent) {
      await Privacy.destroy({});
    }
    var newcontent = await Privacy.create({
      content: inputs.content
    });
    // All done.
    return;
  }
};
