module.exports = {
  friendlyName: "Update Terms",

  description: "Create Terms and conditon",

  inputs: {
    content: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function(inputs) {
    var oldTerms = await Terms.find({
      where: {
        content: inputs.content
      }
    });
    if (oldTerms) {
      await Terms.destroy({});
    }
    var content = await Terms.create({
      content: inputs.content
    });
    // All done.
    // sails.log(this.req.me)
    return;
  }
};
