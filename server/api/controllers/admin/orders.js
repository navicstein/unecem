module.exports = {
  friendlyName: "Orders",

  description: "Orders admin.",

  inputs: {},

  exits: {},

  fn: async function(inputs) {
    var orders = await Order.find()
      .where({})
      .populateAll();
    // All done.
    return orders;
  }
};
