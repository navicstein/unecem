module.exports = {
  friendlyName: "Delete a user in the data base",

  description: "Delete users.",

  inputs: {
    id: {
      type: "string",
      required: true
    }
  },

  exits: {},

  fn: async function({ id }) {
    // All done.
    var user = await User.destroy({ id: id });
    return;
  }
};
