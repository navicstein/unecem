module.exports = {
  friendlyName: "Get users in dashboard",

  description: "Get users.",

  inputs: {},

  exits: {},

  fn: async function(inputs) {
    var users = await User.find().populateAll();
    // .populate("orders")
    // .populate("products");
    // All done.
    return users;
  }
};
