module.exports = {
  friendlyName: "Get analytics",

  description: "",

  inputs: {},

  exits: {},

  fn: async function(inputs) {
    var users = await User.find();
    // console.log("users::", users);
    var products = await Product.find();
    // All done.

    return { users: users.length, products: products.length };
  }
};
