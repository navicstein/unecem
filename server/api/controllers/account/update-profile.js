module.exports = {
  friendlyName: "Update profile",

  description: "Update the profile for the logged-in user.",

  inputs: {
    fullName: {
      type: "string",
      required: true,
      description: "Full representation of the user's name",
      maxLength: 120,
      example: "Lisa Microwave van der Jenny"
    },
    emailAddress: {
      required: true,
      type: "string",
      isEmail: true,
      description: "The email address for the new account, e.g. m@example.com.",
      extendedDescription: "Must be a valid email address."
    },
    oldPassword: {
      type: "string"
      // required: true
    },
    password: {
      description: "The new, unencrypted password.",
      example: "abc123v2"
      // required: true
    }
  },
  exits: {
    success: {
      description: "Up date successfull."
    },

    badCombo: {
      description: `wrong old Password.`,
      // responseType: 'unauthorized'
      statusCode: 406
    }
  },

  fn: async function(inputs) {
    // Hash the new password.
    if (inputs.password && inputs.oldPassword) {
      var hashed = await sails.helpers.passwords.hashPassword(inputs.password);

      var userRecord = await User.findOne({ id: this.req.me.id });

      await sails.helpers.passwords
        .checkPassword(inputs.oldPassword, userRecord.password)
        .intercept("incorrect", "badCombo");
      var UpdateUser = await User.updateOne({ id: this.req.me.id }).set({
        password: hashed,
        fullName: inputs.fullName
      });
    } else {
      sails.log(this.req.session.userId);
      var UpdateUser = await User.update({ id: this.req.me.id }).set({
        fullName: inputs.fullName,
        emailAddress: inputs.emailAddress
      });
    }

    return;
    // Update the record for the logged-in user.
  }
};
