module.exports = {
  friendlyName: "Send email",

  description: "Sails helper to send emails",

  inputs: {
    to: {
      type: "string",
      required: true
    },
    subject: {
      type: "string",
      required: true
    },
    content: {
      type: "string",
      required: true
    },
    from: {
      type: "string",
      required: true
    },
    fullName: { type: "string", required: true }
  },

  exits: {
    success: {
      description: "All done."
    }
  },

  fn: async function(inputs) {
    var nodemailer = require("nodemailer");
    var transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "unecemglobal@gmail.com",
        pass: "une@2019"
      }
    });
    var data = {
      from: inputs.from,
      to: inputs.to,
      subject: inputs.subject,
      text: inputs.content
    };

    sails.log.debug(data);

    transporter.sendMail(data, function(error, info) {
      if (error) {
        sails.log.error(error);
      } else {
        sails.log.info("Email sent: " + info.response);
      }
    });
  }
};
