/**
 * ReqestedPayOut.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    fullName: { type: "string", required: true, maxLength: 120 },
    bankName: { type: "string", required: true },

    accountNumber: { type: "string", required: true },
    amount: {
      type: "number",
      defaultsTo: 0
    },
    refAmount: {
      type: "number",
      defaultsTo: 0
    },
    payed: {
      type: "boolean",
      defaultsTo: false
    },
    user: {
      model: 'user'
    },
    // email: {
    //   type: "string",
    //   required: true,
    //   unique: true,
    //   isEmail: true,
    //   maxLength: 200
    // },
    // phonenumber: { type: "string", required: true },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
  }
};
