/**
 * Products.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    name: {
      type: "string",
      required: true,
      unique: true
    },

    shortName: {
      type: "string",
      required: true,
      unique: true
    },

    description: {
      type: "string",
      required: true,
      description: "the description of this product"
    },

    // category would be dynamic
 

    tags: {
      required: false,
      type: "string"
    },

    subCategory: {
      type: "string",
      description: "the sub category for a product",
      example: "laptop/desktops, phones/tablet"
    },
    price: {
      type: "number",
      required: true
    },
    oldPrice: {
      type: "number",
      required: false
    },

    rating: {
      type: "string"
    },

    availablity: {
      type: "string",
      description: "how many of these in stock",
      example: "65 in stock"
    },

    specifications: {
      type: "json",
      description: "The specs of this product",
      example: "not yet implemented"
    },
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    returnPolicy: {
      type: "string",
      required: false,
      defaultsTo: "No return policy ensured",
      description: "The return policy a product can have",
      example: "^[(60 days money return), (...)]+"
    },
    //
    shipping: {
      type: "number",
      required: false,
    },
    deliveryPrice: {
      type: "number",
      required: false,
    },
    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝
    seo: {
      type: "json",
      required: false,
      defaultsTo: {
        metaTitle: "",
        metaKeywords: "",
        metaDescription: ""
      }
    },

    isActive: {
      type: "boolean",
      required: false,
      defaultsTo: false,
      description: "specifies if this product is active | public | or not ..."
    },

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    photos: {
      collection: "productphotos",
      via: "product"
    },

    reviews: {
      model: "reviews"
    },

    vendor: {
      model: "user",
      description: "The user vendor that has this product[s]"
    },
    category: {
      // collection: "category"
      required: true,
      type: "string",
      description: "the main category for a product",
      example: "Computers and assesories"
    }
  }
};
