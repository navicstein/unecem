/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {
  /***************************************************************************
   *                                                                          *
   * Default policy for all controllers and actions, unless overridden.       *
   * (`true` allows public access)                                            *
   *                                                                          *
   ***************************************************************************/
  "admin/orders": true,
  "cart/overview": true,

  "cart/*": "is-logged-in",
  "admin/*": "is-super-admin",
  "investment/*": "is-logged-in",
  "product/create": ["is-logged-in", "is-super-admin"],
  "bank-info/*": "is-logged-in",
  "requested-pay-out/create": "is-logged-in",
  "requested-pay-out/get": "is-super-admin",
  "orders/delete-order": "is-logged-in",
  "site/upload-images": ["is-logged-in", "is-super-admin"],
  "product/upload-photo": ["is-logged-in", "is-super-admin"]
};
